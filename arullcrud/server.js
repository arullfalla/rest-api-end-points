require('dotenv').config()
var express = require('express')
var app = express()
var cors = require('cors')
var PORT = process.env.PORT || 3030
const bodyParser = require('body-parser')
const config_server = process.env.DB_ATLAS_MONGO || process.env.DB_LOCAL_MONGO


//Install Cors untuk yg nge block fetch.
app.use(cors())


const mongoose = require('mongoose');
mongoose.connect(config_server, {useNewUrlParser: true, useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

app.get('/',(req, res)=>{
    res.send('This is Only EndPoint For BackEnd.')
   
})

app.post('/request', (req,res)=>{
    res.send({
        query: req.query.apaaja
    })
})

app.use(bodyParser.json())

//API
require('./app/routes/user.routes')(app)
require('./app/routes/products.routes')(app)
require('./app/routes/review.routes')(app)

app.listen(PORT, ()=>{
    console.log('Hello, you are running on '+PORT+ ' right now.')
})
